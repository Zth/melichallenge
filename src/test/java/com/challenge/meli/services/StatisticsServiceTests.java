package com.challenge.meli.services;

import com.challenge.meli.model.DnaModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatisticsServiceTests {
    private TestRestTemplate restTemplate;
    private int port;
    private final String statsEndpoint;
    private final String mutantEndpoint;

    StatisticsServiceTests(@Autowired TestRestTemplate restTemplate,
                           @LocalServerPort int port) {
        this.restTemplate = restTemplate;
        this.port = port;
        this.statsEndpoint = "http://localhost:" + port + "/stats";
        this.mutantEndpoint = "http://localhost:" + port + "/mutant";

    }

    @Test
    void testNoStatistics() {
        String actualResponse = restTemplate.getForObject(statsEndpoint, String.class);
        String expectedResponse = "{\"count_human_dna\":0,\"count_mutant_dna\":0,\"ratio\":0.0}";
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    /*@Test
    void testNoHumansStatistics() {
        String[] someMutantDna = new String[]{"AAAAAA", "CTCTCT", "AGAGAG", "CTCTCT", "AGAGAG", "TCTCTC"};
        DnaModel dna = new DnaModel(someMutantDna);
        this.restTemplate.postForEntity(mutantEndpoint, dna, String.class);
        String actualResponse = restTemplate.getForObject(statsEndpoint, String.class);
        String expectedResponse = "{\"count_human_dna\":0,\"count_mutant_dna\":1,\"ratio\":0.0}";
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void testNoMutantsStatistics() {
        String[] someHumanDna = new String[]{"GAAGGA", "CTCTCT", "AGAGAG", "CTCTCT", "AGAGAG", "TCTCTC"};
        DnaModel dna = new DnaModel(someHumanDna);
        this.restTemplate.postForEntity(mutantEndpoint, dna, String.class);
        String actualResponse = restTemplate.getForObject(statsEndpoint, String.class);
        String expectedResponse = "{\"count_human_dna\":1,\"count_mutant_dna\":0,\"ratio\":1.0}";
        assertThat(actualResponse).isEqualTo(expectedResponse);
        for(int i = 0; i < 5; i++) {
            this.restTemplate.postForEntity(mutantEndpoint, dna, String.class);
        }
        actualResponse = restTemplate.getForObject(statsEndpoint, String.class);
        expectedResponse = "{\"count_human_dna\":6,\"count_mutant_dna\":0,\"ratio\":6.0}";
        assertThat(actualResponse).isEqualTo(expectedResponse);

    }*/
}
