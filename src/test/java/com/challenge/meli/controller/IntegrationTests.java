package com.challenge.meli.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testMutantController() {
        final String uri = "http://localhost:" + port + "/isAlive";
        assertThat(this.restTemplate.getForObject(uri, String.class)).contains("OK");
    }

    @Test
    public void testMutantControllerDNA() {
        long startTime = System.currentTimeMillis();
        long stopTime = System.currentTimeMillis();
        System.out.println((stopTime - startTime) + " ms");

        /*WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:" + port)
                .build()
                .get()
                .uri("/isAlive");
        WebTestClient testClient = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:" + port)
                .build();
        testClient.get().uri("/isAlive").exchange().expectStatus().isOk();*/


        /*final String uri = "http://localhost:" + port + "/mutant";
        final String[] dna = new String[]{"CTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCT",
                "TCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTC",
                "AGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAG",
                "TCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTCTC"};
        Genome genome = new Genome();
        genome.setDna(dna);
        this.restTemplate.postForEntity(uri, genome, String.class);

        long startTime = System.currentTimeMillis();
        for(int i = 0; i < 4; i++) {
            this.restTemplate.postForEntity(uri, genome, String.class);
        }
        long stopTime = System.currentTimeMillis();
        System.out.println((stopTime - startTime) + " ms");
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, genome, String.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);*/
    }
}


