package com.challenge.meli;

import com.challenge.meli.controller.MutantController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.*;

import static com.challenge.meli.dna.DnaSolver.isMutant;

@SpringBootTest
class MeliApplicationTests {

	@Autowired
	private MutantController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

	@Test
	void measure(){
		String[] str = new String[]{"AGAGAG", "CTCTCT", "AGAGAG", "CTCTCT", "AGAGAG", "TCTCTC"};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < 1000000; i++) {
			isMutant(str);
		}
		long stopTime = System.currentTimeMillis();
		System.out.println(String.valueOf(isMutant(str)) + " " + (stopTime - startTime) + " ms");
	}
}
