package com.challenge.meli.repositories;

import com.challenge.meli.model.DnaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DnaRepository extends CrudRepository<DnaModel, Long> {
}
