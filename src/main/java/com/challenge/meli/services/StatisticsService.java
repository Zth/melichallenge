package com.challenge.meli.services;

import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class StatisticsService {
    AtomicInteger count_human_dna = new AtomicInteger(0);
    AtomicInteger count_mutant_dna = new AtomicInteger(0);

    public void incrementMutantCounter() {
        this.count_mutant_dna.incrementAndGet();
    }

    public void incrementHumanCounter() {
        this.count_human_dna.incrementAndGet();
    }

    public int getHumanCounter() {
        return this.count_human_dna.get();
    }

    public int getMutantCounter() {
        return this.count_mutant_dna.get();
    }
}
