package com.challenge.meli.services;

import com.challenge.meli.model.DnaModel;
import com.challenge.meli.repositories.DnaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class DnaService {
    @Autowired
    DnaRepository dnaRepository;

    public ArrayList<DnaModel> getAllDNAs() {
        return (ArrayList<DnaModel>)dnaRepository.findAll();
    }

    public void saveDna(Iterable<DnaModel> dna) {
        dnaRepository.saveAll(dna);
    }
}
