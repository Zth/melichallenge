package com.challenge.meli.services;

import com.challenge.meli.model.DnaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Component
public class BufferService {

    Logger logger = LoggerFactory.getLogger(BufferService.class);


    @Autowired
    private DnaService dnaService;

    ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<String> dataToInsert = new ConcurrentLinkedQueue<>();

    public void addToQueue(String[] dna) {
        queue.offer(String.join("", dna));
    }

    public void populateDatabase() {
        if(dataToInsert.peek() != null) {
            logger.error("Something tried to start a batch insert while it was already working.");
            return;
        }
        dataToInsert = queue;
        queue = new ConcurrentLinkedQueue<>();
        logger.info("Inserting " + dataToInsert.size() + " records to the database.");
        //dnaService.saveDna(dataToInsert.stream().map(DnaModel::new).collect(Collectors.toList()));
        dataToInsert = new ConcurrentLinkedQueue<>();
    }

    public boolean isReadyToWork() {
        return (queue.peek() != null) && (dataToInsert.peek() == null);
    }

}
