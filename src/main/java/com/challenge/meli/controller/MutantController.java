package com.challenge.meli.controller;

import com.challenge.meli.job.PersistenceBackgroundJob;
import com.challenge.meli.model.DnaModel;
import com.challenge.meli.model.Stats;
import com.challenge.meli.services.BufferService;
import com.challenge.meli.services.DnaService;
import com.challenge.meli.services.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.challenge.meli.dna.DnaSolver.isMutant;
import static com.challenge.meli.dna.DnaSolver.isValidDna;

@RestController
public class MutantController {

    public final String MUTANT_ENDPOINT = "/mutant";
    public final String STATS_ENDPOINT = "/stats";
    public final String ISALIVE_ENDPOINT = "/isAlive";

    Logger logger = LoggerFactory.getLogger(MutantController.class);

    @Autowired
    private StatisticsService stats;

    @Autowired
    private BufferService bufferService;

    @GetMapping(ISALIVE_ENDPOINT)
    public ResponseEntity<?> isAlive() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = STATS_ENDPOINT, produces = "application/json")
    public Stats getStatistics() {
        return new Stats(stats.getHumanCounter(), stats.getMutantCounter());
    }

    @PostMapping(path = MUTANT_ENDPOINT, consumes = "application/json")
    public ResponseEntity<?> checkMutant(@RequestBody DnaModel genome) {
        String[] dna = genome.getDna();
        if (!isValidDna(dna))
            return new ResponseEntity<>("The DNA syntax is badly formed", HttpStatus.BAD_REQUEST);
        bufferService.addToQueue(dna);
        if(isMutant(dna)) {
            stats.incrementMutantCounter();
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            stats.incrementHumanCounter();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

}
