package com.challenge.meli.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
@Table(name = "dna")
public class DnaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @JsonInclude()
    @Transient
    private String[] dna;

    @Column(unique = false, nullable = false)
    private String parsedDna;

    public DnaModel() { }

    public DnaModel(String[] dna) {
        this.dna = dna;
    }
    public DnaModel(String parsedDna) {
        this.parsedDna = parsedDna;
    }

    public String getParsedDna() {
        return parsedDna;
    }

    public void setParsedDna(String parsedDna) {
        this.parsedDna = parsedDna;
    }

    public String[] getDna() {
        return dna;
    }

    public void setDna(String[] dna) {
        this.dna = dna;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
