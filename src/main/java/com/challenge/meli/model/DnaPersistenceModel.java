package com.challenge.meli.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
@Table(name = "dna")
public class DnaPersistenceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(unique = true, nullable = false)
    private String parsedDna;

    public DnaPersistenceModel(String parsedDna) {
        this.parsedDna = parsedDna;
    }

    public String getParsedDna() {
        return parsedDna;
    }

    public void setParsedDna(String parsedDna) {
        this.parsedDna = parsedDna;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
